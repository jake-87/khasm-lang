# Contributing

Thanks for wanting to contribute!

If you wish to contribute, please ping me on the programminglanguages discord server (#khasm), and we can discuss what you can do.

https://proglangdesign.net/
