# Licensing

Anything in the `src/`, `docs/` or `examples/` is licenced under the LGPL3.0, avalible in `LICENSE_Compiler`.

Anything in the `lib/` directory is licenced under the BSD3, avalible in `LICENCE_Library`