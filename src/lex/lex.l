%{
 #include <stdio.h>
 #include <string.h>
 #include "lex.h"
#include "../parser/parser.h"
#define ret(x) do {yylval.c.t = x; yylval.c.o = o_token; return x;} while (0);
#define set_enum_and_ret(x, y) do {  yylval.c.i.s = strdup(yytext);	\
		yylval.c.i.i = x; \
		yylval.c.o = o_str; \
		if (yylval.c.i.s) {return x;} \
		else {k_error("strdup failed in lex\n");} } while (0);
YYSTYPE yylval;
%}
NUM [0-9]+
FPNUM [+-]?([0-9]*[.])?[0-9]+
STR ["](.*)["]
/*
   This matches for:
   an ident
   a double colon: "::"
   for any amount of times
   then finally:
   an ident

   so this matches:
   a::b::nice__123asd::sujdf

   but these dont:
   a::
   ::b
   1::awd
*/
FUNC [a-zA-Z_][a-zA-Z_0-9]*(([a-zA-Z_][a-zA-Z_0-9]+|[:][:])*[a-zA-Z_][a-zA-Z_0-9]*)?
IDENT [a-zA-Z_]([a-zA-Z0-9_]|[.]|[-][>])*
WSPACE [ \t]*
NEWLINE [\n\r]
SCHAR .
/*these are easy */
%%
"#include"  {ret(t_include)}
"memo"      {ret(t_memo)}
"static"    {ret(t_static)}
"func"      {ret(t_func)}
"using"     {ret(t_using)}
"as"	    {ret(t_as)}
"enum"      {ret(t_enum)}
"struct"    {ret(t_struct)}
"namesace"  {ret(t_namespace)}
"pure"      {ret(t_pure)}
"impure"    {ret(t_impure)}
"if"        {ret(t_if)}
"else"      {ret(t_else)}
"while"     {ret(t_while)}
"for"       {ret(t_for)}
"return"    {ret(t_return)}
"=>"        {ret(t_big_arrow)}
"->"        {ret(t_small_arrow)}
"=="        {ret(t_eq)}
"lambda"    {ret(t_lambda)}
"λ"         {ret(t_lambda)}
"++"	    {ret(t_pp)}
"--"	    {ret(t_mm)}
"+="	    {ret(t_peq)}
"*="	    {ret(t_meq)}
"/="	    {ret(t_deq)}
"%="	    {ret(t_modeq)}
"-="	    {ret(t_mineq)}
"<<"	    {ret(t_bsl)}
">>"	    {ret(t_bsr)}
"<<="	    {ret(t_bsleq)}
">>="	    {ret(t_bsreq) /* sets an enum and returns it, checking for strdup fail */ }
{IDENT}	    {set_enum_and_ret(i_ident);}
{FUNC}      {set_enum_and_ret(i_func);}
{NUM}       {set_enum_and_ret(i_val);}
{FPNUM}     {set_enum_and_ret(i_fval);}
{STR}       {set_enum_and_ret(i_str);}
{WSPACE}    {}
{NEWLINE}   {ret(t_newline)}
{SCHAR}     {yylval.c.c = yytext[0]; yylval.c.o = o_char; return yytext[0];}
<<EOF>>     {return -1;}
%%
