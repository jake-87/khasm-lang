#ifndef LEXH
#define LEXH
enum toks {
  t_memo,
  t_static,
  t_func,
  t_include,
  t_using,
  t_as,
  t_enum,
  t_struct,
  t_namespace,
  t_pure,
  t_impure,
  t_if,
  t_else,
  t_while,
  t_for,
  t_big_arrow,
  t_small_arrow,
  t_return,
  t_pp,   // ++
  t_mm,   // --
  t_eq,   // ==
  t_peq,  // +=
  t_meq,  // *=
  t_deq,  // /=
  t_modeq,// %=
  t_mineq,// -=
  t_bsl,  // <<
  t_bsr,  // >>
  t_bsleq,// <<=
  t_bsreq,// >>=
  t_lambda,
  t_func_def,
  t_newline,
};

enum ident_type {
  i_ident,
  i_func,
  i_val,  // int value
  i_fval, // floating value
  i_str,
};

enum ops {
  o_char,
  o_token,
  o_str,
};
  
struct ident {
  char * s;
  enum ident_type i;
};

/* union can be:
 * char
 * ident
 * token
 * Last `ops` struct is for the type.
 */

struct collect {
  char c;
  struct ident i;
  enum toks t;
  enum ops o;
};

// Union for possible expansion in the future

typedef union YYSTYPE {
  struct collect c;
} YYSTYPE;

void k_error(const char * msg) {
  printf("Khasm error: %s\n", msg);
  exit(1);
}
#endif
